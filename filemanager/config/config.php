<?php
session_start();

$pathToCore = "../../";

require_once($pathToCore."config.inc");
require_once($pathToCore."vendor/autoload.php");

// Pull in constants from the DB
require_once($pathToCore."utils/constants.inc");

use Mumby\WebTools;
use Mumby\WebTools\ACL;

$loggedIn = Mumby\WebTools\ACL\User::isAuthenticated();
if(!$loggedIn)
   die(header("Location: /403"));

$user = Mumby\WebTools\ACL\User::getAuthenticatedUser(_MB_AUTH_TYPE_);

if(!$user->hasPermission(_MB_ROLE_EDIT_WEB_PAGES_))
{
   die(header("Location: /403"));
}

$base_url ="http://".$_SERVER['HTTP_HOST'];  // DON'T TOUCH (base url (only domain) of site (without final /)).
$upload_dir = '/files/'; // path from base_url to base of upload folder (with start and final /)
$current_path = '../files/'; // relative path from filemanager folder to upload folder (with final /)
//thumbs folder can't put inside upload folder
$thumbs_base_path = '../thumbs/'; // relative path from filemanager folder to thumbs folder (with final /)

$confMgr = new Mumby\WebTools\Config(null, _MB_DB_HOST_, _MB_DB_NAME_, _MB_DB_USER_, _MB_DB_PASS_);
$config = $confMgr->getAllConfigs(4);


// OPTIONAL SECURITY
// if set to true only those will access RF whose url contains the access key(akey) like: 
// <input type="button" href="../filemanager/dialog.php?field_id=imgField&lang=en_EN&akey=myPrivateKey" value="Files">
// in tinymce a new parameter added: filemanager_access_key:"myPrivateKey"
// example tinymce config:
// tiny init ...
// 
// external_filemanager_path:"../filemanager/",
// filemanager_title:"Filemanager" ,
// filemanager_access_key:"myPrivateKey" ,
// ...
// add access keys eg: array('myPrivateKey', 'someoneElseKey');
// keys should only containt (a-z A-Z 0-9 \ . _ -) characters
// if you are integrating lets say to a cms for admins, i recommend making keys randomized something like this:
// $username = 'Admin';
// $salt = 'dsflFWR9u2xQa' (a hard coded string)
// $akey = md5($username.$salt);
// DO NOT use 'key' as access key!
// Keys are CASE SENSITIVE!
if(
   isset($config["rm_use_access_keys"]) &&
   isset($config["rm_private_key"]) &&
   isset($config["rm_public_key"]) &&
   !defined("USE_ACCESS_KEYS")
  )
{
   define('USE_ACCESS_KEYS', ($config["rm_use_access_keys"]["defaultValue"] == "true"? true : false)); // TRUE or FALSE
   $access_keys = array($config["rm_private_key"]["defaultValue"],$config["rm_public_key"]["defaultValue"]);
}
else if(!defined("USE_ACCESS_KEYS"))
   define('USE_ACCESS_KEYS', false);


//--------------------------------------------------------------------------------------------------------
// YOU CAN COPY AND CHANGE THESE VARIABLES INTO FOLDERS config.php FILES TO CUSTOMIZE EACH FOLDER OPTIONS
//--------------------------------------------------------------------------------------------------------
if(isset($config["rm_max_upload_size"]))
   $MaxSizeUpload = ((int) $config["rm_max_upload_size"]["defaultValue"]); //Mb
else
   $MaxSizeUpload = 100; //Mb

// SERVER OVERRIDE
if ((int)(ini_get('post_max_size')) < $MaxSizeUpload){
	$MaxSizeUpload = (int)(ini_get('post_max_size'));
}

if(isset($config["rm_language"]))
   $default_language 	= $config["rm_language"]["defaultValue"];
else
   $default_language 	= "en_EN"; //default language file name

if(isset($config["icon_theme"]))
   $icon_theme 		= $config["icon_theme"]["defaultValue"];
else
   $icon_theme 		= "ico"; //ico or ico_dark you can cusatomize just putting a folder inside filemanager/img


//Show or not show folder size in list view feature in filemanager (is possible, if there is a large folder, to greatly increase the calculations)
if(isset($config["show_folder_size"]))
   $show_folder_size = ($config["show_folder_size"]["defaultValue"] == "true"? true : false);
else
   $show_folder_size = true;

//Show or not show sorting feature in filemanager
if(isset($config["show_sorting_bar"]))
   $show_sorting_bar = ($config["show_sorting_bar"]["defaultValue"] == "true"? true : false);
else
   $show_sorting_bar = true;

//Show or not show loading bar
if(isset($config["loading_bar"]))
   $loading_bar = ($config["loading_bar"]["defaultValue"] == "true"? true : false);
else
   $loading_bar = true;

//active or deactive the transliteration (mean convert all strange characters in A..Za..z0..9 characters)
if(isset($config["transliteration"]))
   $transliteration = ($config["transliteration"]["defaultValue"] == "true"? true : false);
else
   $transliteration = false;




//*******************************************
//Images limit and resizing configuration
//*******************************************

// set maximum pixel width and/or maximum pixel height for all images
// If you set a maximum width or height, oversized images are converted to those limits. Images smaller than the limit(s) are unaffected
// if you don't need a limit set both to 0

//active or deactive the transliteration (mean convert all strange characters in A..Za..z0..9 characters)
if(isset($config["image_max_width"]))
   $image_max_width = $config["image_max_width"]["defaultValue"];
else
   $image_max_width = 0;

if(isset($config["image_max_height"]))
   $image_max_height = $config["image_max_height"]["defaultValue"];
else
   $image_max_height = 0;

//Automatic resizing //
// If you set $image_resizing to TRUE the script converts all uploaded images exactly to image_resizing_width x image_resizing_height dimension
// If you set width or height to 0 the script automatically calculates the other dimension
// Is possible that if you upload very big images the script not work to overcome this increase the php configuration of memory and time limit
if(
   isset($config["image_resizing_width"]) ||
   isset($config["image_resizing_height"])
  )
{
   $image_resizing = true;
   if(isset($config["image_resizing_width"]))
      $image_resizing_width = ((int) $config["image_resizing_width"]["defaultValue"]);
   else
      $image_resizing_width = 0;

   if(isset($config["image_resizing_height"]))
      $image_resizing_height = ((int) $config["image_resizing_height"]["defaultValue"]);
   else
      $image_resizing_height = 0;
}
else
{
   $image_resizing = false;
   $image_resizing_width = 0;
   $image_resizing_height = 0;   
}

//******************
// Default layout setting
//
// 0 => boxes
// 1 => detailed list (1 column)
// 2 => columns list (multiple columns depending on the width of the page)
// YOU CAN ALSO PASS THIS PARAMETERS USING SESSION VAR => $_SESSION['RF']["VIEW"]=
//
//******************
if(isset($config["default_view"]))
{
   $default_view = ((int) $config["default_view"]["defaultValue"]);
}
else
{
   $default_view = 0;
}

//set if the filename is truncated when overflow first row 
if(isset($config["truncate_long_filenames"]))
   $ellipsis_title_after_first_row = ($config["truncate_long_filenames"]["defaultValue"] == "true"? true : false);
else
   $ellipsis_title_after_first_row = true;


/******************
 * AVIARY config
*******************/
$aviary_version	= 3;
$aviary_language= 'en';

if(isset($config["aviary_key"]) && isset($config["aviary_key"]))
{
   $aviary_active 	= true;
   $aviary_key 	= "62112f8fe70cac29";
   $aviary_secret	= "7dcb63938873de24";
}
else
{
   $aviary_active 	= false;
   $aviary_key 	= "0";
   $aviary_secret	= "0";
}


//**************************
//Permissions configuration
//**************************
if($user->hasPermission(_MB_ROLE_DELETE_UPLOADED_FILES_))
   $delete_files = true;
else
   $delete_files = false;

if($user->hasPermission(_MB_ROLE_CREATE_UPLOAD_FOLDERS_))
   $create_folders = true;
else
   $create_folders = false;

if($user->hasPermission(_MB_ROLE_DELETE_UPLOAD_FOLDERS_))
   $delete_folders = true;
else
   $delete_folders = false;

if($user->hasPermission(_MB_ROLE_UPLOAD_FILES_))
   $upload_files = true;
else
   $upload_files = false;

if($user->hasPermission(_MB_ROLE_RENAME_UPLOADED_FILES_))
   $rename_files = true;
else
   $rename_files = false;

if($user->hasPermission(_MB_ROLE_RENAME_UPLOADED_FOLDERS_))
   $rename_folders = true;
else
   $rename_folders = false;

if($user->hasPermission(_MB_ROLE_COPY_UPLOADED_FILES_))
   $duplicate_files = true;
else
   $duplicate_files = false;


$includeCopyPasteFunctionality =($config["allow_copy_cut_paste_functionality"]["defaultValue"] == "true"? true : false);

if($user->hasPermission(_MB_ROLE_UPLOAD_FILES_) && $includeCopyPasteFunctionality)
{
   $copy_cut_files = true;
   $copy_cut_dirs	= true;
   
   // cut_max_size defines size limit for paste in MB / operation
   // set 'FALSE' for no limit.
   // cut_max_count defines file count limit for paste / operation
   // set 'FALSE' for no limit.
   // If either of these limits reached, operation won't start and generate warning.
   if(isset($config["copy_cut_max_size"]))
      $copy_cut_max_size	 = $config["copy_cut_max_size"]["defaultValue"];
   else
      $copy_cut_max_size	 = 100;
   if(isset($config["copy_cut_max_count"]))
      $copy_cut_max_count	 = $config["copy_cut_max_count"]["defaultValue"];
   else
      $copy_cut_max_count	 = 200;
}
else
{
   $copy_cut_files = false;
   $copy_cut_dirs	= false;
   $copy_cut_max_size	 = 0;
   $copy_cut_max_count	 = 0;
}

//**********************
//Allowed extensions (lowercase insert)
//**********************

if(isset($config["allowed_image_extensions"]))
{
   $exts = explode(",",$config["allowed_image_extensions"]["defaultValue"]);
   foreach($exts as $k=>$v)
      $exts[$k] = trim($v);
   $ext_img = $exts;
}
else
   $ext_img = array();

if(isset($config["allowed_file_extensions"]))
{
   $exts = explode(",",$config["allowed_file_extensions"]["defaultValue"]);
   foreach($exts as $k=>$v)
      $exts[$k] = trim($v);
   $ext_file = $exts;
}
else
   $ext_file = array();

if(isset($config["allowed_video_extensions"]))
{
   $exts = explode(",",$config["allowed_video_extensions"]["defaultValue"]);
   foreach($exts as $k=>$v)
      $exts[$k] = trim($v);
   $ext_video = $exts;
}
else
   $ext_video = array();

if(isset($config["allowed_music_extensions"]))
{
   $exts = explode(",",$config["allowed_music_extensions"]["defaultValue"]);
   foreach($exts as $k=>$v)
      $exts[$k] = trim($v);
   $ext_music = $exts;
}
else
   $ext_music = array();

if(isset($config["allowed_misc_extensions"]))
{
   $exts = explode(",",$config["allowed_misc_extensions"]["defaultValue"]);
   foreach($exts as $k=>$v)
      $exts[$k] = trim($v);
   $ext_misc = $exts;
}
else
   $ext_misc = array();

$ext = array_merge($ext_img, $ext_file, $ext_misc, $ext_video,$ext_music); //allowed extensions


//The filter and sorter are managed through both javascript and php scripts because if you have a lot of
//file in a folder the javascript script can't sort all or filter all, so the filemanager switch to php script.
//The plugin automatic swich javascript to php when the current folder exceeds the below limit of files number
if(isset($config["js_file_number_limit"]))
   $file_number_limit_js = $config["js_file_number_limit"]["defaultValue"];
else
   $file_number_limit_js = 500;

//**********************
// Hidden files and folders
//**********************
// set the names of any folders you want hidden (eg "hidden_folder1", "hidden_folder2" ) Remember all folders with these names will be hidden (you can set any exceptions in config.php files on folders)
if(isset($config["hidden_folders"]))
{
   $hiddenDirs = explode(",",$config["hidden_folders"]["defaultValue"]);
   foreach($hiddenDirs as $k=>$v)
      $hiddenDirs[$k] = trim($v);
   $hidden_folders = $hiddenDirs;
}
else
   $hidden_folders = array();


// set the names of any files you want hidden. Remember these names will be hidden in all folders (eg "this_document.pdf", "that_image.jpg" )
if(isset($config["hidden_files"]))
{
   $hiddenFiles = explode(",",$config["hidden_files"]["defaultValue"]);
   foreach($hiddenFiles as $k=>$v)
      $hiddenFiles[$k] = trim($v);
   $hidden_files = $hiddenDirs;
}
else
   $hidden_files = array('config.php');


/*******************
 * JAVA upload 
 *******************/
if(isset($config["provide_java_uploads"]))
   $java_upload =($config["provide_java_uploads"]["defaultValue"] == "true"? true : false);
else
   $java_upload = true;

if(isset($config["java_max_upload_size"]))
   $JAVAMaxSizeUpload = ((int) $config["java_max_upload_size"]["defaultValue"]);
else
   $JAVAMaxSizeUpload = 200; //Gb

//************************************
//Thumbnail for external use creation
//************************************
// New image resized creation with fixed path from filemanager folder after uploading (thumbnails in fixed mode)
// If you want create images resized out of upload folder for use with external script you can choose this method, 
// You can create also more than one image at a time just simply add a value in the array
// Remember than the image creation respect the folder hierarchy so if you are inside source/test/test1/ the new image will create at
// path_from_filemanager/test/test1/
// PS if there isn't write permission in your destination folder you must set it


// Activate or not the creation of one or more image resized with fixed path from filemanager folder
if(isset($config["fixed_image_creation"]))
   $fixed_image_creation = ($config["fixed_image_creation"]["defaultValue"] == "true"? true : false);
else
   $fixed_image_creation = false;

// Fixed path of the image folder from the current position on upload folder
if(isset($config["fixed_path_from_filemanager"]))
{
   $paths = explode(",",$config["fixed_path_from_filemanager"]["defaultValue"]);
   foreach($paths as $k=>$v)
      $paths[$k] = trim($v);
   $fixed_path_from_filemanager = $paths;
}
else
   $fixed_path_from_filemanager = array();

// Name to prepend on filenames
if(isset($config["fixed_image_creation_name_to_prepend"]))
{
   $prefix = explode(",",$config["fixed_image_creation_name_to_prepend"]["defaultValue"]);
   foreach($prefix as $k=>$v)
      $prefix[$k] = trim($v);
   $fixed_image_creation_name_to_prepend = $prefix;
}
else
   $fixed_image_creation_name_to_prepend = array();

// Name to append on filename
if(isset($config["fixed_image_creation_to_append"]))
{
   $suffix = explode(",",$config["fixed_image_creation_to_append"]["defaultValue"]);
   foreach($suffix as $k=>$v)
      $suffix[$k] = trim($v);
   $fixed_image_creation_to_append = $suffix;
}
else
   $fixed_image_creation_to_append = array();

// Width of image (you can leave empty if you set height)
if(isset($config["fixed_image_creation_width"]))
{
   $fixedWidths = explode(",",$config["fixed_image_creation_width"]["defaultValue"]);
   foreach($fixedWidths as $k=>$v)
      $fixedWidths[$k] = trim($v);
   $fixed_image_creation_width = $fixedWidths;
}
else
   $fixed_image_creation_width = array();

// Height of image (you can leave empty if you set width)
if(isset($config["fixed_image_creation_height"]))
{
   $fixedHeights = explode(",",$config["fixed_image_creation_height"]["defaultValue"]);
   foreach($fixedHeights as $k=>$v)
      $fixedHeights[$k] = trim($v);
   $fixed_image_creation_height = $fixedHeights;
}
else
   $fixed_image_creation_height = array();


// New image resized creation with relative path inside to upload folder after uploading (thumbnails in relative mode)
// With Responsive filemanager you can create automatically resized image inside the upload folder, also more than one at a time
// just simply add a value in the array
// The image creation path is always relative so if i'm inside source/test/test1 and I upload an image, the path start from here

// Activate or not the creation of one or more image resized with relative path from upload folder
if(isset($config["relative_image_creation"]))
   $relative_image_creation_height = ($config["relative_image_creation"]["defaultValue"] == "true"? true : false);
else
   $relative_image_creation = false;

// Relative path of the image folder from the current position on upload folder
if(isset($config["relative_path_from_current_pos"]))
{
   $paths = explode(",",$config["relative_path_from_current_pos"]["defaultValue"]);
   foreach($paths as $k=>$v)
      $paths[$k] = trim($v);
   $relative_path_from_current_pos = $paths;
}
else
   $relative_path_from_current_pos = array();

// Name to prepend on filenames
if(isset($config["relative_image_creation_name_to_prepend"]))
{
   $prefix = explode(",",$config["relative_image_creation_name_to_prepend"]["defaultValue"]);
   foreach($prefix as $k=>$v)
      $prefix[$k] = trim($v);
   $relative_image_creation_name_to_prepend = $prefix;
}
else
   $relative_image_creation_name_to_prepend = array();

// Name to append on filename
if(isset($config["relative_image_creation_name_to_append"]))
{
   $suffix = explode(",",$config["relative_image_creation_name_to_append"]["defaultValue"]);
   foreach($suffix as $k=>$v)
      $suffix[$k] = trim($v);
   $relative_image_creation_name_to_append  = $suffix;
}
else
   $relative_image_creation_name_to_append  = array();

// Width of image (you can leave empty if you set height)
if(isset($config["relative_image_creation_width"]))
{
   $relWidths = explode(",",$config["relative_image_creation_width"]["defaultValue"]);
   foreach($relWidths as $k=>$v)
      $relWidths[$k] = trim($v);
   $relative_image_creation_width = $relWidths;
}
else
   $relative_image_creation_width = array();

// Height of image (you can leave empty if you set width)
if(isset($config["relative_image_creation_height"]))
{
   $relHeights = explode(",",$config["relative_image_creation_height"]["defaultValue"]);
   foreach($relHeights as $k=>$v)
      $relHeights[$k] = trim($v);
   $relative_image_creation_height = $relHeights;
}
else
   $relative_image_creation_height = array();

?>