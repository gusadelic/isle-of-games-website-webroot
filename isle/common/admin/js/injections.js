$(document).ready( function()
{
   $('.hiddenInjectionOption').hide();
});

function toggleInjectionTypes(el)
{
   thisVal = el.val();
   if(thisVal == 3)
   {
      // Show people data and hide custom options
      $('#fieldWrapper_250').show();
      $('#fieldWrapper_249').hide();
   }
   else
   {
      // Hide people data and show custom options
      $('#fieldWrapper_249').show();
      $('#fieldWrapper_250').hide()
   }
}