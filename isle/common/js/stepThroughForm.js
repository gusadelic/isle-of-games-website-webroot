(function($)
{
   var stepThroughCallbacks = new Array();
   
   $.fn.stepThroughForm = function(options)
   {
      var fieldsets;
      this.each(function()
      {
         var theForm = $(this);
         var maxHeight = "";

         fieldsets = theForm.children("fieldset.mainWrapper");
         
         fieldsets.each( function(i)
         {
            buttons = "";
            // Add a 'Prev' button
            if(i !== 0)
               buttons = buttons + '<a href="#" id="prev_'+i+'" data-prev="'+(i-1)+'" class="prev btn btn-primary">Back</a> ';
            
            // Add a 'Next' button
            if(i !== (fieldsets.length-1))
               buttons = buttons + '<a href="#" id="next_'+i+'" data-next="'+(i-1)+'" class="next btn btn-primary">Next</a> ';
            else
               buttons = buttons + '<a href="#" class="submit btn btn-primary">Submit</a>';

            // Append prev/next buttons
            $(this).append('<p class="stepThroughButtons" style="text-align:center">'+buttons+'</p>');

            // Users can always go back to previous fieldsets in the form
            $(this).find('.stepThroughButtons .prev.btn').click( function(e)
            {
               e.preventDefault();
               stepThrough.stepBack(i-1);
            });

            // Users can only go forward if they've filled out all
            // required fields within a given fieldsets.
            $(this).find('.stepThroughButtons .next.btn').click( function(e)
            {
               e.preventDefault();
               var validFields = true;
               $(this).parents('fieldset.mainWrapper').find('input,textarea,select').each( function()
               {
                  thisField = $('[name='+$(this).attr('name')+']');
                  if(!thisField[0].checkValidity())
                  {
                     validFields = false;
                     thisField.parents('div.fieldWrapper').addClass('invalidField');
                  }
                  else
                  {
                     thisField.parents('div.fieldWrapper').removeClass('invalidField');
                  }
               });
               
               // Need to do another run through for required checkbox options
               $(this).parents('fieldset.mainWrapper').find('fieldset.pickFrom.required').each( function()
               {
                  thisSet = true;
                  if($(this).find('input:checked').length == 0)
                     thisSet = false;
                  
                  if(!thisSet)
                  {
                     validFields = false;
                     thisField.parents('div.fieldWrapper').addClass('invalidField');
                  }
                  else
                  {
                     thisField.parents('div.fieldWrapper').removeClass('invalidField');
                  }
               });

               if(validFields)
                  stepThrough.stepForward(i+1);
            });
            
            $(this).find('.stepThroughButtons .submit.btn').click( function(e)
            {
               e.preventDefault();
               theForm.submit();
            });

            $(this).hide();
         });
      });
      
      this.find('input[type=submit]').hide();
      
      this.addCallback = function(stepNum, callback)
      {
         stepThroughCallbacks[stepNum] = callback;
      }
      
      this.removeCallback = function(stepNum)
      {
         stepThroughCallbacks[stepNum] = null;
      }
      
      this.stepForward = function(stepNum)
      {
         if(stepThroughCallbacks[stepNum] != "undefined" && stepThroughCallbacks[stepNum] != null)
         {
            stepThroughCallbacks[stepNum]();
         }
         else
         {
            fieldsets.each( function() { $(this).hide(); });
            $(fieldsets[stepNum]).show();
         }
      };
      
      this.stepBack = function(stepNum)
      {
         fieldsets.each( function() { $(this).hide(); });
         $(fieldsets[stepNum]).show();         
      }

      this.stepForward(0);
      return this;
   };

})(jQuery);