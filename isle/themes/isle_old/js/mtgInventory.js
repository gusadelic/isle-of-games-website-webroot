var api = "http://localhost:8081";
var main = "http://localhost:8080";

/*
$(document).on("ready", function() {

    readyStockModal();
    
        $("#resulttable")
        .on( 'order.dt',  function () { readyStockModal(); } )
        .on( 'search.dt', function () { readyStockModal(); } )
        .on( 'page.dt',   function () { readyStockModal(); } )
        .dataTable();
});


function readyStockModal() {
    $('tr').on('click', function() {
        var target_name = $(this).find('.Name_cell').html();
        var target_set = $(this).find('.Set_cell').html();
        var target_id = $(this).find('.id_cell').html();
        //var stockTable = $(this).find('.stockTable_cell').html();
        var stockData = JSON.parse($(this).find('.stockData_cell').html());
        var addForm = $(this).find('.addForm_cell').html();

        var stockTable = '<table cellpadding="0" cellspacing="0" border="0" class="display" id="stockTable"></table>';
        $('#stockTable').append("<thead><tr><th>Condition</th><th>Language</th><th>Group</th><th>Stock</th><th>Price</th><thead>");
        
        for ( var key in stockData ) {
            stockData[key].Group = stockData[key].Group.replace(/\"/g, "");
            console.log(stockData[key].Group);
        }
        
        $("#inventoryModal").modal();
        $("#inventoryModal").find('.modal-title').html(target_name + " - " + target_set);
        $("#inventoryModal").find('.modal-body').html(stockTable + addForm);

        $('#stockTable').DataTable( {
            "className":      "small",
            "autoWidth": true,
            "searching": false,
            "data": stockData,
            "columns": [
                { "title": "Condition", "data": "Condition" },
                { "title": "Language", "data": "Language" },
                { "title": "Group", "data": function(o, val) { 
                    return $("<div/>").html(o.Group).text();
                    } },
                { "title": "Stock", "data": function(o, val) { 
                    return $("<div/>").html(o.Stock).text();
                    }, "class": "center" },
                { "title": "Price","data": function(o, val) { 
                    return $("<div/>").html(o.Price).text();
                    }, "class": "center" }
            ]
        });
       
    
    });
    
}

*/

function getPrice(id, foil, callback) {
    var url = main + "/app/magic/price";
    var data = { 'card_id' : id, 'foil' : foil };
    return $.get(url, data, callback ); 
}

function togglePrice(target, id) {
    var add_form = $('#add_form_' + id);
    var price_form = add_form.find('input.price_input');
    
    price_form.val("");
    price_form.addClass('facebook-small');
    
    var price_data;

    var lang = add_form.find('select.lang_select').val();
    var cond = add_form.find('select.cond_select').val();

    if ( target.is(':checked') ) {
        getPrice( id, 1, function(data) { 
            price_data = JSON.parse(data);    
            if ( price_data == null ) var fair_price = "0.00";
            else var fair_price = price_data.fair_price;
            price_form.removeClass('facebook-small');
            price_form.val( accounting.formatMoney( parseFloat(fair_price).toFixed(2) * lang * cond, { format: "%v" }) );
        });
    }
    else {
        getPrice( id, 0, function(data) { 
            price_data = JSON.parse(data);
            if ( price_data == null ) var fair_price = "0.00";
            else var fair_price = price_data.fair_price;
            price_form.removeClass('facebook-small');
            price_form.val( accounting.formatMoney( parseFloat(fair_price).toFixed(2) * lang * cond, { format: "%v" }) );
        });
    }
    
}

function deleteStock(id) {
    var button = $('#delButton_' + id);
    button.html("_____");
    button.prop('disabled', 'true');
    button.addClass('facebook-small');
       
    var url = main + "/app/magic/stock/del";
    var data = { 'id': id };
    $.post(url, data, function(response) { 
        if (response==="success" && !response.error) {
            //button.removeClass('facebook-small');
            //button.prop('disabled', 'false');
            //button.html("Delete");
            window.location.reload(true);
        }

    });
}

function modStock(id) {
    var button = $('#modButton_' + id);
    button.html("____");
    button.prop('disabled', 'true');
    button.addClass('facebook-small');
    
    var row = button.closest('tr');
    var group = row.find('.group_select').val();
    var stock = row.find('input.stock').val();
    var price = row.find('input.price_input').val();
       
    var url = main + "/app/magic/stock/mod";
    var data = { 'id': id, 'group': group, 'stock': stock, 'price': price };
    $.post(url, data, function(response) { 
        if (response==="success" && !response.error) {
            //button.removeClass('facebook-small');
            //button.prop('disabled', 'false');
            //button.html("Modify");
            window.location.reload(true);
        }
    });
}

function addStock(card_id) {
    var button = $('#addButton_' + card_id);
    button.html("____");
    button.prop('disabled', 'true');
    button.addClass('facebook-small');
    
    var add_form = $('#add_form_' + card_id);
    
    var condition = add_form.find('.cond_select').val();
    var language = add_form.find('.lang_select').val();
    var group = add_form.find('.group_select').val();
    var stock = add_form.find('.stock_input').val();
    var price = add_form.find('.price_input').val();
    var foil = 0;
    
    if ( add_form.find('.new_foil').is(':checked') ) {
        foil = 1;
    }
       
    var url = main + "/app/magic/stock/add";
    var data = { 'card_id': card_id, 'condition': condition, 'language': language, 'group': group, 'stock': stock, 'foil': foil, 'price': price, 'javascript': "yes" };
    $.post(url, data, function(response) { 
        if (response==="success" && !response.error) {
            
            //button.removeClass('facebook-small');
            //button.prop('disabled', 'false');
            //button.html("Add");
            window.location.reload(true);
        }
    });
}

function quickAddStock(card_id, defaultGroup) {
    var button = $('#quickAddButton_' + card_id);
    button.html("_____");
    button.prop('disabled', 'true');
    button.addClass('facebook-small');
    
    var condition = 1;
    var language = 1;
    var group = defaultGroup;
    var stock = 1;
    var price = $('#marketprice_' + card_id + '').html();
    var foil = 0;
       
    var url = main + "/app/magic/stock/add";
    var data = { 'card_id': card_id, 'condition': condition, 'language': language, 'group': group, 'stock': stock, 'foil': foil, 'price': price, 'javascript': "yes" };
    $.post(url, data, function(response) { 
        if (response==="success" && !response.error) {

            button.closest('td').html('<span class="hidden">1</span>In Stock! ');
            //window.location.reload(true);
        }
    });
}


