$(document).ready( function() {
   $('.sortableTable').dataTable({
      "bPaginate": false,
      "bLengthChange": false,
      "bFilter": true,
      "bSort": true,
      "bInfo": false,
      "bAutoWidth": false,
      "oLanguage": { "sSearch": "Filter:" }
   });
});
