
$(document).ready(function() {

   $('#moreResearchDetails').hide();
   
   $("#moreResearchDetailsLink").click( function() {
      $('#moreResearchDetails').toggle(250, function(){
         
         if($(this).is(":visible"))
         {
            $("#moreResearchDetailsLink").text("Show less...");
         }
         else
         {
            $("#moreResearchDetailsLink").text("Show more...");
         }
      });
   });
});