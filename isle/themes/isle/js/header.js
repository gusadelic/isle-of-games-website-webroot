//function init() {
//    
//    window.addEventListener('scroll', function(e){
//        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
//            shrinkOn = 200,
//            
//            carousel = document.querySelector("div.carousel");
//        if (distanceY > shrinkOn) {
//            classie.add(header,"smaller");
//            classie.add(carousel,"hidden");
//        } else {
//            if (classie.has(header,"smaller")) {
//                classie.remove(header,"smaller");
//            }
//            if (classie.has(carousel,"hidden")) {
//                classie.remove(carousel,"hidden");
//            }
//        }
//    });
//}
//
//$(document).ready( function() {
//    header = document.querySelector("header"); 
//    init();
//});

var cbpAnimatedHeader = (function() {

	var docElem = document.documentElement,
		header = document.querySelector( 'header' ),
		didScroll = false,
		changeHeaderOn = 100;

	function init() {
		window.addEventListener( 'scroll', function( event ) {
			if( !didScroll ) {
				didScroll = true;
				setTimeout( scrollPage, 50 );
			}
		}, false );
	}

	function scrollPage() {
		var sy = scrollY();
		if ( sy >= changeHeaderOn ) {
			classie.add( header, 'smaller' );
		}
		else {
			classie.remove( header, 'smaller' );
		}
		didScroll = false;
	}

	function scrollY() {
		return window.pageYOffset || docElem.scrollTop;
	}

	init();

})();