var api = "http://localhost:8081";
var main = "http://localhost:8080";

window.onload = function() { drawSets('expansion', $('#expansions') ); }

function getSets(type, callback) {
    var url = main + "/app/magic/sets/" + type;
    return $.get(url, {}, callback );
}

function drawSets(type, target) {
    var sets;

    target.html("");
    
    if ( type === 'expansion') {
        var count_promo = 0;
        var promo_group = 0;
        getSets(type, function(data) { 
            sets = JSON.parse(data); 
            var blocks = [];
            for ( var i = 0 ; i < sets.length ; i++  ) {
                var thisBlock = sets[i].block.replace("'", "").replace(/ /g, "_");
                 if (blocks.indexOf(thisBlock) === -1 && thisBlock !== '')
                    blocks.push(thisBlock);
             }

            for ( var i = blocks.length-1 ; i > -1 ; i--  ) {
                target.append('<div id="' + blocks[i] + '" class="list-group"></div>');
            }
            for ( var i = 0 ; i < sets.length ; i++  ) {
                var name = sets[i].name;
                var code = sets[i].code;
                var logo = sets[i].logo;
                var icon = '<img src="' + sets[i].icon + '">';

                if ( logo === "" ) logo = '<h5>' + name + '</h5>';
                else logo = '<img src="' + logo + '">';
                    
                if ( sets[i].block === "Promo" ) {
                    
                    if ( count_promo !== 0 ) {
                        if ( count_promo % 3 === 0 ) {
                            promo_group ++;
                            target.append('<div id="Promo_' + promo_group + '" class="list-group"></div>');
                        }
                        if ( promo_group === 0 ) {
                            target.find('#Promo').append('<a id="' + sets[i].code + '" href="/admin/magic/browse/' + sets[i].code + '" class="list-group-item">' + icon + logo + '<span class="releaseDate"><h4>Released about ' + moment(sets[i].releaseDate).fromNow() + ' </h4><span></a>');          
                        }
                        else {
                            target.find('#Promo_' + promo_group).append('<a id="' + sets[i].code + '" href="/admin/magic/browse/' + sets[i].code + '" class="list-group-item">' + icon + logo + '<span class="releaseDate"><h4>Released about ' + moment(sets[i].releaseDate).fromNow() + ' </h4><span></a>');        
                        }
                    }
                    else {
                        target.find('#Promo').append('<a id="' + sets[i].code + '" href="/admin/magic/browse/' + sets[i].code + '" class="list-group-item">' + icon + logo + '<span class="releaseDate"><h4>Released about ' + moment(sets[i].releaseDate).fromNow() + ' </h4><span></a>');        
                    }
                    count_promo++;
                }
                else {
                    target.find('#' + sets[i].block.replace("'", "").replace(/ /g, "_")).append('<a id="' + sets[i].code + '" href="/admin/magic/browse/' + sets[i].code + '" class="list-group-item">' + icon + logo + '<span class="releaseDate"><h4>Released about ' + moment(sets[i].releaseDate).fromNow() + ' </h4><span></a>');        
                }
            }
        });
    }
    else if ( type === 'core' ) {
        getSets(type, function(data) { 
            sets = JSON.parse(data); 
            var group = 1;
            target.html('<div id="group_' + group + '" class="list-group"></div>');
            for ( var i = sets.length - 1 ; i > -1  ; i--  ) {
                var name = sets[i].name;
                var code = sets[i].code;
                var logo = sets[i].logo;
                var icon = '<img src="' + sets[i].icon + '">';

                if ( logo === "" ) logo = '<h5>' + name + '</h5>';
                else logo = '<img src="' + logo + '">';
                
                if ( i !== 0 && i % 3 === 0) { 
                    group++; 
                    target.append('<div id="group_' + group + '" class="list-group"></div>'); 
                }
                
                target.find("#group_" + group).append('<a id="' + code + '" href="/admin/magic/browse/' + sets[i].code + '" class="list-group-item">' + icon + logo + '<span class="releaseDate"><h4>Released about ' + moment(sets[i].releaseDate).fromNow() + ' </h4><span></a>');        
            }
        });
    }
    else if ( type === 'duel deck') {
        getSets(type, function(data) { 
            sets = JSON.parse(data); 
            var group = 1;
            target.html('<div id="dual_group_' + group + '" class="list-group"></div>');
            for ( var i = sets.length - 1 ; i > -1  ; i--  ) {
                var name = sets[i].name;
                var code = sets[i].code;
                var logo = sets[i].logo;
                var icon = '<img src="' + sets[i].icon + '">';

                if ( logo === "" ) logo = '<h5>' + name + '</h5>';
                else logo = '<img src="' + logo + '">';
                
                if ( i % 3 === 0) { 
                    group++; 
                    target.append('<div id="dual_group_' + group + '" class="list-group"></div>'); 
                }
 
                target.find("#dual_group_" + group).append('<a id="' + sets[i].code + '" href="/admin/magic/browse/' + sets[i].code + '" class="list-group-item">\n' + icon + logo + '<span class="releaseDate"><h4>Released about ' + moment(sets[i].releaseDate).fromNow() + ' </h4><span></a>');        
            }
        });        
    }
    else if ( type === 'from the vault') {
        getSets(type, function(data) { 
            sets = JSON.parse(data); 
            var group = 1;
            target.html('<div id="vault_group_' + group + '" class="list-group"></div>');
                
            for ( var i = sets.length - 1 ; i > -1  ; i--  ) {
                var name = sets[i].name;
                var code = sets[i].code;
                var logo = sets[i].logo;
                var icon = '<img src="' + sets[i].icon + '">';

                if ( logo === "" ) logo = '<h5>' + name + '</h5>';
                else logo = '<img src="' + logo + '">';                
                
                if ( i % 3 === 0) { 
                    group++; 
                    target.append('<div id="vault_group_' + group + '" class="list-group"></div>'); 
                }
 
                target.find("#vault_group_" + group).append('<a id="' + sets[i].code + '" href="/admin/magic/browse/' + sets[i].code + '" class="list-group-item" >' + icon + logo + '<span class="releaseDate"><h4>Released about ' + moment(sets[i].releaseDate).fromNow() + ' </h4><span></a>');        
            }
        });        
    }
    else if ( type === 'multiplayer') {
        getSets(type, function(data) { 
            sets = JSON.parse(data); 
            var group = 1;
            target.html('<div id="multi_group_' + group + '" class="list-group"></div>');
            for ( var i = sets.length - 1 ; i > -1  ; i--  ) {
                var name = sets[i].name;
                var code = sets[i].code;
                var logo = sets[i].logo;
                var icon = '<img src="' + sets[i].icon + '">';

                if ( logo === "" ) logo = '<h5>' + name + '</h5>';
                else logo = '<img src="' + logo + '">';
                
                if ( i % 3 === 0) { 
                    group++; 
                    target.append('<div id="multi_group_' + group + '" class="list-group"></div>'); 
                }
 
                target.find("#multi_group_" + group).append('<a id="' + sets[i].code + '" href="/admin/magic/browse/' + sets[i].code + '" class="list-group-item" >' + icon + logo + '<span class="releaseDate"><h4>Released about ' + moment(sets[i].releaseDate).fromNow() + ' </h4><span></a>');        
            }
        });        
    }
    else if ( type === 'special') {
        getSets(type, function(data) { 
            sets = JSON.parse(data); 
            var group = 1;
            target.html('<div id="special_group_' + group + '" class="list-group"></div>');
            for ( var i = sets.length - 1 ; i > -1  ; i--  ) {
                var name = sets[i].name;
                var code = sets[i].code;
                var logo = sets[i].logo;
                var icon = '<img src="' + sets[i].icon + '">';

                if ( logo === "" ) logo = '<h5>' + name + '</h5>';
                else logo = '<img src="' + logo + '">';
                
                if ( i % 3 === 0) { 
                    group++; 
                    target.append('<div id="special_group_' + group + '" class="list-group"></div>'); 
                }
                
                target.find("#special_group_" + group).append('<a id="' + sets[i].code + '" href="/admin/magic/browse/' + sets[i].code + '" class="list-group-item" >' + icon + logo + '<span class="releaseDate"><h4>Released about ' + moment(sets[i].releaseDate).fromNow() + ' </h4><span></a>');        
            }
        });        
    }

    
}