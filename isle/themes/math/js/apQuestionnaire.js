var totalExamsTaken = 0;
var upcomingSemester;
var ab = false;
var bc = false;
var st = false;
var no = false;

var lastBackButton;

$(document).ready( function()
{
   now = new Date();
   thisMonth = parseInt(now.getMonth());
   
   if(thisMonth < 8)
      upcomingSemester = "Fall";
   else
      upcomingSemester = "Spring";
   
   // Hide file upload until they need it.
   $('#fieldWrapper_field_191').hide();

   // Force them to upload their file if they've
   // received their scores.
   $("input[name='field_190']").click( function(event)
   {
      if($(this).val() == 1)
      {
         $('#fieldWrapper_field_191').hide();
         $('#field_191').removeAttr('required');
         $('label[for=field_191]').removeClass('required');
         
         $("#examScoresInstructions").html("What score(s) do you expect to receive on your AP exam(s)?")
      }
      else
      {
         $('#fieldWrapper_field_191').show();
         $('#field_191').attr('required','required');
         $('label[for=field_191]').addClass('required');
         
         $("#examScoresInstructions").html("What score(s) did you receive on your AP exam(s)?")
      }
   });

   // Make sure that if they specify that they've taken an exam,
   // they can't also specify that they've taken 'None of the above'.
   $("#fieldWrapper_field_192 input[type=checkbox]").not("#field_192_option_NO").click( function()
   {
      if($(this).is(":checked"))
      {
         $("#field_192_option_NO").removeAttr('checked');
      }
   });
   
   // If they check 'None of the Above' for exams, then it should
   // clear any other checkboxes they've made.
   $("#field_192_option_NO").click( function()
   {
      if($(this).is(":checked"))
      {
         $(this).parent().siblings('div').children('input').removeAttr('checked');
      }
   });
   
   // Adjust last back button to go to the right place.
   $('#prev_6').click( function(){
      // Move on to the next panel.
      nextStep(lastBackButton);
   });
   
   $("input[name=field_206]").click( function()
   {
      $("#field_207").attr("value",$(this).val());
   });
});


/*
 * Depending on what the user checks for what exams
 * they've taken, we need to either adjust the form
 * or submit the form (if they're done).
 */
function checkExamsTaken()
{
   totalExamsTaken = 0;
   
   ab = false;
   bc = false;
   st = false;
   no = false;
   
   scoreLabels = new Array();
   
   $('#fieldWrapper_field_192 input[type=checkbox]:checked').each(function()
   {
      val = $(this).val();
      console.debug(val);

      switch(val)
      {
         case "AB":
            ab = true;
            scoreLabels.push("AB Exam Score:");
            totalExamsTaken++;
            break;
         case "BC":
            bc = true;
            scoreLabels.push("BC Exam Score:");
            totalExamsTaken++;
            break;
         case "ST":
            st = true;
            scoreLabels.push("Statistics Exam Score:");
            totalExamsTaken++;
            break;
         case "NO":
            no = true;
            break;
         default:
            break;
      }
   });
   
   // Submit the form if they didn't take any exams.
   if(!ab && !bc)
   {
      if(st)
         $("#field_207").attr("value","statsOnly");
      else
         $("#field_207").attr("value","noExams");

      forceSubmit();
   }
   else
   {
      existing = $("#fieldWrapper_field_194");
      existing.show();
      
      // Clear out the old form fields.
      existing.parent('.mainWrapper').find('.removableInput').remove();
         
      // Add new form fields for each exam.
      for(a = 0 ; a < totalExamsTaken; a++)
      {
         newInput = existing.clone();
         newInput.attr("id", "fieldWrapper_194_"+a);
         newInput.addClass("removableInput");
         
         thisLabel = newInput.children('label');
         thisLabel.attr('for', 'field_194_'+a);
         thisLabel.addClass('required', 'required');
         thisLabel.text(scoreLabels[a]);
         select = newInput.find('div select');
         select.attr('name', 'field_194_'+a);
         select.attr('id', 'field_194_'+a);
         select.attr('required', 'required');
         newInput.insertBefore(existing.parent('.mainWrapper').children('.stepThroughButtons'));
      }
      
      existing.hide();

      nextStep(4);
   }
}

function checkExamScores()
{
   maxTest = 0;
   index = 0;
   
   if(ab)
   {
      abScore = parseInt($('#field_194_'+index).val());
      if(abScore >= 3)
         maxTest = 1;
      
      index++;
   }
   
   if(bc)
   {
      bcScore = parseInt($('#field_194_'+index).val());
      if(bcScore >= 3)
         maxTest = 1;
      if(bcScore >= 4)
         maxTest = 2;
   }
   
   adjustmentWarning = " After registering for classes, if you later find that your score is not high enough, it is your responsibility to adjust your schedule on your own through UAccess. Student who do not meet the course requirements will be dropped prior to the start of classes.";
   
   if(maxTest == 0)
   {
      // The user isn't eligible for any AP credit.
      $("#field_207").attr("value","noPass");

      forceSubmit();
   }
   else
   {

      if(maxTest == 1)
      {
         setLastPageInstructions("129");
         updateLastPageOptions("129");
         lastBackButton = 4;
         nextStep(6);
      }
      else
      {
         instructions = "Based on the information provided, you ";
         missingScores = $("input[name='field_190']:checked").val();

         if(missingScores == 1)
            instructions = instructions + "may be";
         else
            instructions = instructions + "are";

         instructions = instructions + " eligible for Calculus II (MATH 129) or Vector Calculus (MATH 223). Please indicate which course you're interested in below.";
         $('#courseInstructions').html(instructions);

         lastBackButton = 5;
         nextStep(5);
      }
   }
}

function checkRequestedCourse()
{
   option = $('input[name=field_203]:checked').val();
   switch(option)
   {
      case "129":
         setLastPageInstructions("129");
         updateLastPageOptions("129");
         nextStep(6);
         break;
      case "223":
         setLastPageInstructions("223");
         updateLastPageOptions("223");
         nextStep(6);
         break;
      case "ADV":
         $("#field_206_option_ADV").attr("checked","checked");
         $("#field_207").attr("value","ADV");
         forceSubmit();
         break;
      default:
         break;
   }
}

// Depending on when the user's orientation date is,
// we make slight changes to the text.
function checkOrientationDate()
{
   // Remove old text
   $('#statsWarning .tagOn').remove();
   
   now = new Date();
   orientationDate = Date.parse($('#field_189 option:selected').text());
   if(now.getTime() > orientationDate)
      $('#statsWarning').append("<span class='tagOn'> that you received at your orientation.</span>");
   else
      $('#statsWarning').append("<span class='tagOn'>  that you will receive at your orientation.</span>");

   // Move on to the next panel.
   nextStep(2);
}

function nextStep(next)
{
   fieldsets = $('#mb_form_17').children("fieldset.mainWrapper");
   fieldsets.each( function() { $(this).hide(); });
   $(fieldsets[next]).show();
}

function setLastPageInstructions(course)
{
   if(course == "129")
      optionsInstructions = "In order to be eligible for MATH 129, you need Calculus I credit (AB 3 or higher, BC 3 or higher). ";
   else
      optionsInstructions = "In order to be eligible for MATH 223, you need Calculus II credit (BC 4 or higher). ";

   missingScores = $("input[name='field_190']:checked").val();
   if(missingScores == 1)
      optionsInstructions = optionsInstructions + adjustmentWarning;

   if(course == "129")
      optionsInstructions = optionsInstructions + "We do have an honors section of MATH 129. ";
   else
      optionsInstructions = optionsInstructions + "We do have an honors section of MATH 223. ";

   optionsInstructions = optionsInstructions + "See the <a href='/academics/courses/honors'>Math Department honors website</a> for more information.<br /><br />";
   optionsInstructions = optionsInstructions + "Please indicate which course you're interested in below.";

   $('#preferenceInstructions').html(optionsInstructions);
}

function updateLastPageOptions(course)
{
   if(course == "223")
   {
      $("#field_206_option_223").parent('div').show();
      $("#field_206_option_223H").parent('div').show();
      $("label[for=field_206_option_NO").text("I am not interested in taking Calculus II (Math 129) or Vector Calculus (Math 223) at this time.");
   }
   else
   {
      $("#field_206_option_223").parent('div').hide();
      $("#field_206_option_223H").parent('div').hide();
      $("label[for=field_206_option_NO").text("I am not interested in taking Calculus II (Math 129) at this time.");
   }
}

function forceSubmit()
{
   $("#mb_form_17 [required=required]").removeAttr("required");
   $("#mb_form_17").submit();
}