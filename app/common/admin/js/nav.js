var currentIndex = -1;

$(document).ready( function() {
   
   // If we only have one item (root node), we have to
   // make a few adjustments.
   nodes = $('ol.sortable>li').length;
   if(nodes == 1)
   {
      sublists = $('ol.sortable>li>ol').length;
      if(sublists == 0)
       $('ol.sortable>li').append("<ol></ol>");
   }
   
	$('ol.sortable>li>ol').nestedSortable( {
		disableNesting: 'no-nest',
		forcePlaceholderSize: true,
		handle: 'i',
		helper: 'clone',
		items: 'li',
      minLevel:2,
		opacity: .6,
		placeholder: 'placeholder',
		revert: 250,
		tabSize: 25,
		tolerance: 'pointer',
		toleranceElement: '> div'
	});

	$('#submit').click( function(e) {
		arraied = $('ol.sortable>li>ol').nestedSortable('serialize');
		$('#serialized').val(arraied);
		$('#navForm').submit();
	})
});

function addNavItem()
{
	currentIndex = $('ol.sortable>li>ol').find('li').length;
   currentIndex++;
   
   newNode = '<li id="menu_'+currentIndex+'">'+
             '<div>'+
             '<i class="fa fa-sort-amount-asc glyphicon glyphicon-sort"></i>'+
             '<span class="deleteMe fa fa-ban glyphicon glyphicon-remove-circle" onclick="javascript:deleteItem('+currentIndex+'); return false;"></span>'+
             '<input name="menu_'+currentIndex+'_id" value="" type="hidden" /> '+
             '<input name="menu_'+currentIndex+'_attr" value="" type="hidden" /> '+
             '<input name="menu_'+currentIndex+'_text" value="Menu Text" type="text" /> '+
             '<input name="menu_'+currentIndex+'_link" value="Link URL" type="text" />'+
             '</div>'+
             '</li>';

	$('ol.sortable>li>ol').prepend(newNode);
	$('#emptyNav').hide();
	$('ol.sortable>li>ol').sortable('refresh');
}

function deleteItem(item)
{
	$('#menu_'+item).remove();
	$('ol.sortable').sortable('refresh');
}