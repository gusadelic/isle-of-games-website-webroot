var main = "http://" + location.host;

var item_count = 0;
var cards;
var conditions;
var languages;
var extDialog;
var max_results = 5;

$(document).on("ready", function() {
    
    String.prototype.contains = function(it) { return this.indexOf(it) != -1; };
    
    getConditions();
    getLanguages();
    
    $("#buy_link").on('click', function() {
        
        BootstrapDialog.show({
            size: BootstrapDialog.SIZE_WIDE,
            title: 'Transaction - Buy',
            message: function(dialog) { 
                return buy_dialog;               
            },
            onshown: function(dialogRef){
                var keyup = 0;
                var tmp = null;
                var results = null;
                
                $('button.buy-button').prop('disabled', true);
                
                $('#searchbox').on( "focusin", function() {
                    $('#selected_card').remove();
                    $('#searchbox').val("");
                    $('#search_results').html("");
                });
                
                $(document).click( function() {
                    $('#searchbox').val("");
                    $('#search_results').html("");
                });
                
                //$('#add-buy-item').on('click', function() {
                selectCard = function(index) {
                    //if ( !$('#selected_card').attr('index') ) {
                    //    BootstrapDialog.alert("You must choose a card from the search list.");
                    //}
                    //else {

                        //var this_card = cards[$('#selected_card').attr('index')];
                        var this_card = cards[index];

                        if ( this_card.name.length > 26 ) {
                            var name = this_card.name.substring(0,26) + "...";
                        }
                        else {
                            var name = this_card.name;
                        }
                        
                        var stats = getStats(this_card);
                        
                        if ( this_card.hasFoil === "0" ) {
                            var disabled = ' disabled';
                        }
                        else {
                            var disabled = '';
                        }

                        var quantity = '<input class="form-control input-sm stock_input" type="number" min="1" value="1" onchange="updateTotals();"></input>';
                        var price = '<div class="input-group price_input_container"><span class="input-group-addon input-sm">$</span><input class="form-control input-sm price_input" type="text" value="' + accounting.formatMoney( parseFloat(this_card.fair_price).toFixed(2) * 0.5, { format: "%v" }) + '"></input><span class="input-group-addon input-sm"><input type="checkbox" class="new_foil" aria-label="Foil" onchange="togglePrice($(this),' + this_card.id + ', updateTotals );"' + disabled + '> Foil</span></div>';

                        $('#buy-list').append('<li class="list-group-item" card_id="' + this_card.id + '" market_price="' + this_card.fair_price + '"><span class="del-item blank" onclick="delItem($(this))">----</span><span class="list-group-item-heading card_name">'+ name +'</span> - <span class="list-group-item-text card_set">'+ this_card.setname +'</span> <span class="stats">' + stats + '</span> <br /><span class="buy_item" id="add_form_' + this_card.id + '">'+ conditions + ' ' + languages + ' ' + quantity + ' ' + price + '</span>  <span class="item-total-container">Item Total: <span class="item-total"> ' + accounting.formatMoney( parseFloat(this_card.fair_price).toFixed(2) * 0.5, { format: "%v" }) + '</span></span> </li>');
                       
                        $('.price_input').on('change', function() { updateTotals(); });
                        updateTotals();
                        
                    //}
                };
                
                $("input").each(function(){
                    $(this).attr("autocomplete", "off");
                });
                
                $("#searchbox").keyup( function(event) {
                    $('#search_results').html("");
                    keyup = 1;
                    if ( tmp == null ) {
                            countdown();
                    }
                });
                
                function countdown() {
                    if ( keyup == 0 ) {
                            window.clearTimeout(tmp);
                            tmp = null;
                            search();
                    }
                    else {
                            $('#searchbox').addClass('facebook-small');
                            keyup--;
                            tmp = window.setTimeout(countdown,600);
                    }
                }
                
                function search() {

                    $.ajax({
                        type: "GET",
                        url: "/app/magic/search",
                        data: 'query=' + $("#searchbox").val(),
                        dataType: "json"
                    }).done( function(result) {  
                        $('#searchbox').removeClass('facebook-small');
                        $('#search_results').html("");
                        
                        cards = result;
                        var skip_count = 0;

                        $.each(cards, function(index, card) {
                            if ( cards[index].number.contains('b') ) {
                                skip_count++;
                                return true;
                            }
                            
                            var stockText = "Out of Stock!";
                            var foilText = "";

                            if ( card.foil_count == 1 ) {
                                foilText = ', ' + card.foil_count +' foil in stock';
                                stockText = "";
                            }
                            else if ( card.foil_count > 1 ) {
                                foilText = ', ' + card.foil_count +' foils in stock';
                                stockText = "";
                            }

                            if ( card.count > 0 )
                                stockText = card.count +' in stock';
                            
                            $("#search_results").append('<a href="#" class="list-group-item" onclick="selectCard('+index+');"><h4 class="list-group-item-heading">' + cards[index].name + '</h4>' + cards[index].setname + ' (' + stockText + foilText + ')</a>');
                            return ( (index - skip_count + 2) <= max_results);
                        });

                        if ( ( cards.length - skip_count + 1 ) > max_results )
                        $("#search_results").append('<a href="#" class="list-group-item" onclick="showAllResults(\''+ $("#searchbox").val() + '\');">View all... (' + (cards.length - skip_count - max_results ) + ' more) </a>');


                        if ( $("#searchbox").val() === "" ) {
                            $("#search_results").html(""); 
                        }
                        else if ( !cards[0] ) {
                            $("#search_results").html('<div class="list-group-item">No Matches!</div>');
                        }

                    });
                }
                

            },
            buttons: [
            {
                icon: 'glyphicon glyphicon-usd',
                label: 'Buy',
                cssClass: 'btn-primary buy-button',
                action: function(dialog){
                    if ( $('#t_id').val() === "" ) {
                        BootstrapDialog.alert("Transaction ID required!");
                    }
                    else {
                        var data = { 'type': 'buy', 'vend_id': $('#t_id').val(), 'notes': $('#notes').val(), 'items': {} };
                        data.items = getBuyItems();
                        
                        performTransaction(data, function(result) {
                            dialog.enableButtons(true);
                            if ( result == 0 ) { 
                                BootstrapDialog.alert("Transaction Complete!", function() {
                                    window.location.reload(true);     
                                });
                            }
                            else if ( result == 2 ) {
                                dialog.enableButtons(true);
                                BootstrapDialog.alert("There was a problem with your Transaction.  Make sure you entered a unique Transaction ID!");
                            }
                            else {
                                BootstrapDialog.confirm('There was a problem with the Transaction.  Would you like to try again?', function(result){
                                    if(result) {
                                        
                                    }else {
                                        window.location.reload(true);
                                    }
                                });
                            }
                            
                        });

                    }
                }
            }, {
                label: 'Cancel',
                action: function(dialogItself){
                    dialogItself.close();
                }
            }]
        });
        
    });

    $("#sell_link").on('click', function() {

        BootstrapDialog.show({
            size: BootstrapDialog.SIZE_WIDE,
            title: 'Transaction - Sell',
            message: function(dialog) { 
                return sell_dialog;               
            },
            onshown: function(dialogRef){
                var keyup = 0;
                var tmp = null;
                var results = null;
                
                $('button.sell-button').prop('disabled', true);
                
                $('#searchbox').on( "focusin", function() {
                    $('#selected_card').remove();
                    $('#searchbox').val("");
                    $('#search_results').html("");
                });
                
                $(document).click( function() {
                    $('#searchbox').val("");
                    $('#search_results').html("");
                });
                
                //$('#add-sell-item').on('click', function() {
                selectCard = function(index) {
//                    if ( !$('#selected_card').attr('index') ) {
//                        BootstrapDialog.alert("You must choose a card from the search list.");
//                    }
                    //else {
                        
                        var this_card = cards[index];

                        if ( this_card.name.length > 26 ) {
                            var name = this_card.name.substring(0,26) + "...";
                        }
                        else {
                            var name = this_card.name;
                        }
                        
                        //var stats = getStats(this_card);
                        
                        var foil = "";
                        if ( this_card.foil == 1 ) foil = '<span class="foil">Foil</span>';

                        var quantity = '<input class="form-control input-sm stock_input" type="number" min="1" max="' + this_card.count + '" value="1" onchange="updateTotals();"></input>';
                        var price = '<span class="item_price">' + accounting.formatMoney( parseFloat(this_card.price).toFixed(2), { format: "%v" }) + '</span>';

                        $('#sell-list').append('<li class="list-group-item" card_id="' + this_card.id + '" set_code="' + this_card.set + '" cond_id="'+ this_card.cond_id +'" lang_id="'+ this_card.lang_id +'" foil="' + this_card.foil + '" group_id="' + this_card.group_id + '" price="' + this_card.price + '" ><span class="del-item blank" onclick="delItem($(this))">----</span><span class="list-group-item-heading card_name">'+ name +'</span> - <span class="list-group-item-text card_set" >'+ this_card.setname +'</span> <span class="sell_item"> ' + foil + ' <span class="condition">'+ this_card.cond + '</span><span class="language">' + this_card.lang + '</span>  from <span class="group">' + this_card.group_name + '</span><br /><span class="market_value"> Market Value: ' + this_card.fair_price + '</span> Selling price: ' + price + '</span> Qty: ' + quantity + ' <span class="item-total-container">Item Total: <span class="item-total"> ' + accounting.formatMoney( parseFloat(this_card.price).toFixed(2), { format: "%v" }) + '</span></span> </li>');
                       
                        $('.price_input').on('change', function() { updateTotals(); });
                        updateTotals();
                        
                        $('#selected_card').remove();
                        
                    //}
                };
                
                $("input").each(function(){
                    $(this).attr("autocomplete", "off");
                });
                
                $("#searchbox").keyup( function(event) {
                    $('#search_results').html("");
                    keyup = 1;
                    if ( tmp == null ) {
                            countdown();
                    }
                });
                
                function countdown() {
                    if ( keyup == 0 ) {
                            window.clearTimeout(tmp);
                            tmp = null;
                            search();
                    }
                    else {
                            $('#searchbox').addClass('facebook-small');
                            keyup--;
                            tmp = window.setTimeout(countdown,600);
                    }
                }
                
                function search() {

                    $.ajax({
                        type: "GET",
                        url: "/app/magic/stock",
                        data: 'query=' + $("#searchbox").val(),
                        dataType: "json"
                    }).done( function(result) {  
                        $('#searchbox').removeClass('facebook-small');
                        $('#search_results').html("");
                        
                        cards = result;
                        var skip_count = 0;

                        $.each(cards, function(index, card) {
                            if ( isSelected(card) === true || cards[index].number.contains('b') ) {
                                skip_count++;
                                return true;
                            }
                            
                            var stockText = "Out of Stock!";
                            var foilText = "";

//                            if ( card.foil_count == 1 ) {
//                                stockText = ', ' + card.foil_count +' foil in stock';
//                                stockText = "";
//                            }
//                            else if ( card.foil_count > 1 ) {
//                                foilText = ', ' + card.foil_count +' foils in stock';
//                                stockText = "";
//                            }

                            if ( card.count > 0 )
                                stockText = card.count +' in stock';
                      
                            $("#search_results").append('<a href="#" class="list-group-item" onclick="selectCard('+index+');"><h4 class="list-group-item-heading">' + card.card_name + '</h4>' + cards[index].setname + '<br /> (' + card.cond_code + " " + card.lang + " - " + card.group_name + ', ' + stockText + foilText + ')</a>');
                            return ( (index - skip_count + 2) <= max_results );
                        });
                        
                        if ( ( cards.length - skip_count + 1 ) > max_results )
                        $("#search_results").append('<a href="#" class="list-group-item" onclick="showAllResults(\''+ $("#searchbox").val() + '\');">View all... (' + (cards.length - skip_count - max_results ) + ' more) </a>');


                        if ( $("#searchbox").val() === "" ) {
                            $("#search_results").html(""); 
                        }
                        else if ( !cards[0] ) {
                            $("#search_results").html('<div class="list-group-item">No Matches!</div>');
                        }

                    });
                }
                

            },
            buttons: [
            {
                icon: 'glyphicon glyphicon-usd',
                label: 'Sell',
                cssClass: 'btn-primary sell-button',
                action: function(dialog){

                    if ( $('#t_id').val() === "" ) {
                        BootstrapDialog.alert("Transaction ID required!");
                    }
                    else {

                        dialog.enableButtons(false);
                        var data = { 'type': 'sell', 'vend_id': $('#t_id').val(), 'notes': $('#notes').val(), 'items': {} };

                        data.items = getSellItems();
                        
                        performTransaction(data, function(result) {
                            dialog.enableButtons(true);
                            if ( result == 0 ) { 
                                BootstrapDialog.alert("Transaction Complete!", function() {
                                    window.location.reload(true);     
                                });
                            }
                            else if ( result == 2 ) {
                                dialog.enableButtons(true);
                                BootstrapDialog.alert("There was a problem with your Transaction.  Make sure you entered a unique Transaction ID!");
                            }
                            else {
                                BootstrapDialog.confirm('There was a problem with the Transaction.  Would you like to try again?', function(result){
                                    if(result) {
                                        
                                    }else {
                                        window.location.reload(true);
                                    }
                                });
                            }
                            
                        });

                    }                                               
                }
            }, {
                label: 'Cancel',
                action: function(dialogItself){
                    dialogItself.close();
                }
            }]
        });

    });
    
    
    
});


function getBuyItems() {
    var items = [];
    $('#buy-list li').each(function( index ) {
        if ( $(this).find('input.new_foil').is(':checked') ) var foil = 1;
        else var foil = 0;
        
        var tempItem = { "card_id": $(this).attr('card_id'),
                      "condition": $(this).find('select.cond_select').val(),
                      "language": $(this).find('select.lang_select').val(),
                      "group": 1,
                      "foil": foil,
                      "qty": $(this).find('.stock_input').val(),
                      "buy_price": $(this).find('.price_input').val(),
                      "price": $(this).attr("market_price") 
        };

        items.push( tempItem );

    });
    return items;
}


function getSellItems() {
    var items = [];
    $('#sell-list li').each(function( index ) {
        var tempItem = { "card_id": $(this).attr('card_id'),
                      "condition": $(this).attr('cond_id'),
                      "language": $(this).attr('lang_id'),
                      "group": $(this).attr('group_id'),
                      "foil": $(this).attr('foil'),
                      "qty": $(this).find('.stock_input').val(),
                      "price": $(this).attr('price') };

        items.push( tempItem );

    });
    return items;
}

function performTransaction(data, callback) {

    var transaction = JSON.stringify(data); 

    $.ajax({
        type: "POST",
        url: "/app/magic/transaction",
        data: "data=" + transaction
    }).success( function(answer) {
        if ( answer.contains('success') ) { 
            callback(0);
        }
        else {
            callback(1);
        }
    }).error( function() {
        callback(2);
    });    
    
    
}


function showAllResults(query) {
     var results = '<ul id="all_results" class="list-group">';
     $.each(cards, function(index, card) {
        if ( isSelected(card) === true || card.number.contains('b') ) {
            return true;
        }
         
        var stockText = "Out of Stock!";
        var foilText = "";
        
        if ( card.foil_count == 1 ) {
            foilText = ', ' + card.foil_count +' foil in stock';
            stockText = "";
        }
        else if ( card.foil_count > 1 ) {
            foilText = ', ' + card.foil_count +' foils in stock';
            stockText = "";
        }
        
        if ( card.count > 0 )
            stockText = card.count +' in stock';
        
        
        
        results += '<a href="#" class="list-group-item" onclick="selectCard('+index+'); extDialog.close();"><h4 class="list-group-item-heading">' + card.card_name + '</h4>' + card.setname + ' (' + stockText + foilText + ')</a>';
     });
     results += "</ul>"; 
     BootstrapDialog.alert({
        title: 'Results for "' + query + '"',
        message: function(dialog) { extDialog = dialog; return results; }
     });         
}
    


                        
//function selectCard(index) {
//    $('#searchbox').val("");
//    $('#search_results').html("");
//    if ( cards[index].name.length > 26 ) {
//        var name = cards[index].name.substring(0,26) + "...";
//    }
//    else {
//        var name = cards[index].name;
//    }
//    $('.bootstrap-dialog-message').prepend('<div class="label label-success" id="selected_card" index="'+index+'">' + name + ' - ' + cards[index].set + '</div>' );
//    
//}



function delItem(target) {
    target.parent().remove();
    updateTotals();
}

function updateTotals() {
    var total = 0;
    var items = 0;
    
    $('.stock_input').each( function() {
       items += +$(this).val(); 
    });
    $('span.item-total').each( function() {
        var price_input_element = $(this).parent().parent().find('.price_input');

       if ( price_input_element.length > 0 )
        var price = $(this).parent().parent().find('.price_input').val().replace(',','');
       else 
        var price = $(this).parent().parent().attr('price');

       var qty = $(this).parent().parent().find('.stock_input').val();
       var subtotal = parseFloat(price) * parseFloat(qty);

       $(this).html( accounting.formatMoney( parseFloat(subtotal).toFixed(2))); 
       total += subtotal;
    });
    
    $('#item-count').html(items);
    $('#total').html(accounting.formatMoney( parseFloat(total).toFixed(2), { format: "%v" }));
    
    if ( $('#buy-list li').length === 0 ) {
        $('button.buy-button').prop('disabled', true);
    }
    else {
        $('button.buy-button').prop('disabled', false);
    }
    
    if ( $('#sell-list li').length === 0 ) {
        $('button.sell-button').prop('disabled', true);
    }
    else {
        $('button.sell-button').prop('disabled', false);
    }
    
}

function getStats(price) {
    
    if ( !price || !price.absoluteChangeSinceYesterday ) {
        var oneday = '<span class="change-value down">' + accounting.formatMoney("0.00", { format: "%v" }) + '</span>';
    }
    else if ( price.absoluteChangeSinceYesterday.contains('-') ) {
        var oneday = '<span class="change-value down">' + accounting.formatMoney( parseFloat(price.absoluteChangeSinceYesterday).toFixed(2), { format: "%v" }) + '</span>';
    }
    else if ( price.absoluteChangeSinceYesterday === '0' ) {
        var oneday = '<span class="change-value">' + accounting.formatMoney( parseFloat(price.absoluteChangeSinceYesterday).toFixed(2), { format: "%v" }) + '</span>';  
    }
    else {
        var oneday = '<span class="change-value up">+' + accounting.formatMoney( parseFloat(price.absoluteChangeSinceYesterday).toFixed(2), { format: "%v" }) + '</span>';
    }

    if ( !price || !price.absoluteChangeSinceOneWeekAgo ) {
        var oneweek = '<span class="change-value down">' + accounting.formatMoney("0.00", { format: "%v" }) + '</span>';
    } 
    else if ( price.absoluteChangeSinceOneWeekAgo.contains('-') ) {
        var oneweek = '<span class="change-value down">' + accounting.formatMoney( parseFloat(price.absoluteChangeSinceOneWeekAgo).toFixed(2), { format: "%v" }) + '</span>';
    }
    else if ( price.absoluteChangeSinceOneWeekAgo === '0' ) {
        var oneweek = '<span class="change-value">' + accounting.formatMoney( parseFloat(price.absoluteChangeSinceOneWeekAgo).toFixed(2), { format: "%v" }) + '</span>';  
    }
    else {
        var oneweek = '<span class="change-value up">+' + accounting.formatMoney( parseFloat(price.absoluteChangeSinceOneWeekAgo).toFixed(2), { format: "%v" }) + '</span>';
    }

    if ( !price || !price.bestVendorBuylistPrice ) {
        var bestbuy = accounting.formatMoney( "0.00", { format: "%v" });
    }
    else {
        var bestbuy = accounting.formatMoney( parseFloat(price.bestVendorBuylistPrice).toFixed(2), { format: "%v" });
    }

    return 'Stats <span class="stats-explanation">(1 day / 7 day / High)</span>: '+ oneday +' / '+ oneweek + ' / ' + bestbuy;
}

function isSelected(item) {
    result = false;
    var items = getSellItems();
    
    if ( items.length === 0 ) return result;
    
    $.each(items, function(i,s) {
       if ( item.card_id === s.card_id 
         && item.cond_id === s.condition
         && item.lang_id === s.language
         && item.group_id === s.group
         && item.foil === s.foil
         && item.price === s.price ) {

         result = true;
         return true;
       }
    });
    
    return result;
}

function getConditions() {
    $.ajax({
        type: "POST",
        url: "/app/magic/conditions" 
    }).done( function(result) { 
        conditions = result;
    });
}

function getLanguages() {
    $.ajax({
        type: "POST",
        url: "/app/magic/languages" 
    }).done( function(result) { 
        languages = result;
    });
}


var buy_dialog = '<ul id="search_results" class="list-group"></ul>'
               + '<form class="form-inline"><div class="dialog-title form-group"><input type="text" name="search" class="search form-control" id="searchbox" placeholder="Search"></div>'
               + '<div class="metadata-container form-group"> <label for="t_td">Transaction ID</label> <input type="text" name="t_id" id="t_id" class="form-control" required> <label for="notes">Notes</label> <input type="text" name="notes" id="notes" class="form-control"></div>'
               + '<div class="list-container"><ul class="list-group" id="buy-list"></ul></div>'
               + '<div class="stats-container"><h4 style="display: inline;">Number of items: <span id="item-count"></span></h4>  <div class="total"><h2 style="display: inline;">Grand Total: $<span id="total">0.00</span></div></div></form>';
       
var sell_dialog = '<ul id="search_results" class="list-group"></ul>'
               + '<form class="form-inline"><div class="dialog-title form-group"><input type="text" name="search" class="search form-control" id="searchbox" placeholder="Search"></div>'
               + '<div class="metadata-container form-group"> <label for="t_td">Transaction ID</label> <input type="text" name="t_id" id="t_id" class="form-control" required> <label for="notes">Notes</label> <input type="text" name="notes" id="notes" class="form-control"></div>'
               + '<div class="list-container"><ul class="list-group" id="sell-list"></ul></div>'
               + '<div class="stats-container"><h4 style="display: inline;">Number of items: <span id="item-count"></span></h4>  <div class="total"><h2 style="display: inline;">Grand Total: $<span id="total">0.00</span></div></div></form>';
       



