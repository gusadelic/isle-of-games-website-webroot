$(document).ready( function() {
   $('table').each( function() {
      if(!$(this).hasClass("table"))
         $(this).addClass("table");
//      if(!$(this).hasClass("table-striped") && !$(this).hasClass("noStripes"))
//         $(this).addClass("table-striped");
//      if(!$(this).hasClass("table-bordered") && !$(this).hasClass("noBorders"))
//         $(this).addClass("table-bordered");
        if($(this).hasClass("noStripes"))
            $(this).removeClass("table-striped");
        if($(this).hasClass("addBorders"))
            $(this).addClass("table-bordered");
   });
});
