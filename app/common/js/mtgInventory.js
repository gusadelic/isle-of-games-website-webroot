var main = "http://" + location.host;


$(document).on("ready", function() {

    String.prototype.contains = function(it) { return this.indexOf(it) != -1; };
    
    // Update Groups
    $('.group_select').each(function(i,t) {
        if ( i > 0 ) 
            $(this).val( $(this).parent().attr('group') ); 
    }); 
    
    $('#showsets_link').on('click', function(e) {
        e.preventDefault();
        showSets();
    });
    
    $('#showcond_link').on('click', function(e) {
        e.preventDefault();
        showConditions();
    });
    
    $('#showlang_link').on('click', function(e) {
        e.preventDefault();
        showLanguages();
    });
    
    // DOUBLE FACED CARDS
    // Handle double-faced card flipping functionality.
    $(".btn-flip-image").on('click', function() {
        $(this).parent().parent().toggleClass('flipped');
    });
});

function showSets() {
   
   BootstrapDialog.show({
            title: 'Magic: The Gathering Sets',
            message: '<table id="AllSets" class="display" width="100%"></table>',
            size: 'size-wide', 
            closable: true, // <-- Default value is false
            draggable: true, // <-- Default value is false
            buttonLabel: 'OK', // <-- Default value is 'OK',
            callback: function(result) {

            },
            onshown: function() {
                var table = $('#AllSets').dataTable( {
                    data: sets,
                    columns: [
                        { data: "Icon" },
                        { data: "Code" },
                        { data: "Name" },
                        { data: "Release Date" }
                    ]
                });
                
                //var order = table.order();
                
                // Sort by column 1 and then re-draw
                //table
                //    .order( [ 3, 'desc' ] )
                //    .draw();
            }
    });
  
}

function showConditions() {

    var table = $('#condtable').dataTable();
    var order = table.order();
    
    // Sort by column 1 and then re-draw
    table
        .order( [ 0, 'desc' ] )
        .draw();
   
   BootstrapDialog.show({
            title: 'Magic: The Gathering Conditions',
            message:  $('#condTable').clone().removeClass('hidden'),
            size: 'size-wide', 
            closable: true, // <-- Default value is false
            draggable: true, // <-- Default value is false
            buttonLabel: 'OK', // <-- Default value is 'OK',
            callback: function(result) {

            }
    });
  
}

function showLanguages() {

    var table = $('#langtable').dataTable();
    var order = table.order();
    
    // Sort by column 1 and then re-draw
    table
        .order( [ 0, 'desc' ] )
        .draw();
   
   BootstrapDialog.show({
            title: 'Magic: The Gathering Languages',
            message: $('#langTable').clone().removeClass('hidden'),
            size: 'size-wide', 
            closable: true, // <-- Default value is false
            draggable: true, // <-- Default value is false
            buttonLabel: 'OK', // <-- Default value is 'OK',
            callback: function(result) {

            }
    });
  
}

function deleteLanguage(id, button) {
    button.html("____");
    button.prop('disabled', 'true');
    button.addClass('facebook-small');
    var url = main + "/app/magic/languages/delete";
    var data = { 'id': id };
    $.post(url, data, function() {
        window.location.reload(true);
    });
}

function deleteCondition(id, button) {
    button.html("____");
    button.prop('disabled', 'true');
    button.addClass('facebook-small');
    var url = main + "/app/magic/conditions/delete";
    var data = { 'id': id };
    $.post(url, data, function() {
        window.location.reload(true);
    });
}

function deleteGroup(id, button) {
    button.html("____");
    button.prop('disabled', 'true');
    button.addClass('facebook-small');
    var url = main + "/app/magic/groups/delete";
    var data = { 'id': id };
    $.post(url, data, function() {
        window.location.reload(true);
    });
}

function getPrice(id, foil, callback) {
    var url = main + "/app/magic/price";
    var data = { 'card_id' : id, 'foil' : foil };
    return $.get(url, data, callback ); 
}


function togglePrice(target, id, callback) {
    
    if ( $('#buy-list').length === 0 ) {
        half = 1;
        var add_form = target.parent().parent().parent().parent().parent();
    }
    else {
        half = 0.5;
        var add_form = target.parent().parent().parent();
    }
    //var add_form = $('#add_form_' + id);  
    
    var price_form = add_form.find('input.price_input:first');
    
    price_form.val("");
    price_form.addClass('facebook-small');
    
    var price_data;

    var lang = add_form.find('select.lang_select').val();
    var cond = add_form.find('select.cond_select').val();

    if ( target.is(':checked') ) {
        getPrice( id, 1, function(data) { 
            price_data = JSON.parse(data);    
            if ( price_data == null ) var fair_price = "0.00";
            else var fair_price = price_data.fair_price;
            price_form.removeClass('facebook-small');
            price_form.val( accounting.formatMoney( parseFloat(fair_price).toFixed(2) * lang * cond * half, { format: "%v" }) );
            if ( callback instanceof Function ) {
               callback();
               $('#add_form_' + id).parent().find('.stats').html(getStats(price_data));
               $('#add_form_' + id).parent().attr('market_price', accounting.formatMoney( parseFloat(fair_price).toFixed(2) * lang * cond, { format: "%v" }));
            }

        });
    }
    else {
        getPrice( id, 0, function(data) { 
            price_data = JSON.parse(data);
            if ( price_data == null ) var fair_price = "0.00";
            else var fair_price = price_data.fair_price;
            price_form.removeClass('facebook-small');
            price_form.val( accounting.formatMoney( parseFloat(fair_price).toFixed(2) * lang * cond * half, { format: "%v" }) );
            if ( callback instanceof Function ) {
               callback();
               $('#add_form_' + id).parent().find('.stats').html(getStats(price_data));
               $('#add_form_' + id).parent().attr('market_price', accounting.formatMoney( parseFloat(fair_price).toFixed(2) * lang * cond, { format: "%v" }));
            }

        });
        

    }
    
}


// function deleteStock(id) {
//     var button = $('#delButton_' + id);
//     button.html("_____");
//     button.prop('disabled', 'true');
//     button.addClass('facebook-small');
//     var allbox = $('all_box+' + id);

//     if ( allbox.is('checked') )  var all = 1;
//     else var all = 0;
       
//     var url = main + "/app/magic/stock/del";
//     var data = { 'id': id, 'all': all };
//     $.ajax({
//         method: "POST",
//         url: url,
//         data: data
//     }).done( function() {
//         window.location.reload(true);
//     });
// }

function deleteStock(id, cardid) {
    var button = $('#delButton_' + id);
    button.html("____");
    button.prop('disabled', 'true');
    button.addClass('facebook-small');
    
    var row = button.closest('tr');

    var condition = row.find('.condition_id').attr('value');
    var language = row.find('.language_id').attr('value');
    var group = row.find('.group_select option:selected').attr('id');
    var stock = row.find('.stock').html();
    var price = row.find('input.price_input').val();
    var foilbox = row.find('input.inventory_foil');
    var allbox = row.find('input.all_box');

    if ( foilbox.is(':checked') ) var foil = 1;
    else var foil = 0;

    if ( allbox.is(':checked') )  var all = 1;
    else var all = 0;    
       
    var url = main + "/app/magic/stock/del";
    var data = { 'id': id, 'card_id': cardid, 'condition': condition, 'language': language, 'group': group, 'price': price, 'foil': foil, 'qty': stock, 'all': all };
    $.ajax({
        method: "POST",
        url: url,
        data: data
    }).done( function() {
        window.location.reload(true);
    });
}

function modStock(button, id, cardid) {
    button.html("________");
    button.prop('disabled', 'true');
    button.addClass('facebook-small');
    
    var row = button.closest('tr');
    var condition = row.find('.condition_id').attr('value');
    var language = row.find('.language_id').attr('value');
    var group = row.find('.group_select option:selected').attr('id');
    var price = row.find('input.price_input').val();
    var foilbox = row.find('input.inventory_foil');
    var allbox = row.find('input.all_box');
    var foil = 0;
    var all = 0;

    if ( foilbox.is(':checked') ) foil = 1;

    if ( allbox.is(':checked') )  all = 1;
   
       
    var url = main + "/app/magic/stock/mod";
    var data = { 'id': id, 'card_id': cardid, 'cond_id': condition, 'lang_id': language, 'group_id': group, 'price': price, 'foil': foil, 'all': all };
    $.ajax({
        method: "POST",
        url: url,
        data: data
    }).done( function() {
        window.location.reload(true);
    });
}

function addStock(card_id, type) {
    var button = $('#addButton_' + card_id);
    button.html("____");
    button.prop('disabled', true);
    button.addClass('facebook-small');
    
    var add_form = $('#add_form_' + card_id);
    
    var condition = add_form.find('.cond_select option:selected').attr('id');
    var language = add_form.find('.lang_select option:selected').attr('id');
    var group = add_form.find('.group_select option:selected').attr('id');
    var stock = add_form.find('.stock_input').val();
    var price = add_form.find('.price_input').val();
    var foil = 0;
    
    if ( add_form.find('.new_foil').is(':checked') ) {
        foil = 1;
    }
       
    var url = main + "/app/magic/stock/add";
    var data = { 'card_id': card_id, 'condition': condition, 'language': language, 'group': group, 'stock': stock, 'foil': foil, 'price': price, 'type': type };
 
    $.ajax({
        method: "POST",
        url: url,
        data: data
    }).done( function() {
        window.location.reload(true);
    });
}

function quickAddStock(card_id, defaultGroup, previous) {
    var button = $('#quickAddButton_' + card_id);
    var gridbutton = $('#quickAddButtonGrid_' + card_id);
    //button.html("________");
    gridbutton.prop('disabled', true);
    gridbutton.addClass('facebook-small');
    button.prop('disabled', true);
    button.addClass('facebook-small');
    
    var condition = 1;
    var language = 1;
    var group = defaultGroup;
    var stock = 1;
    var price = $('#marketprice_' + card_id + '').html();
    var foil = 0;
    var type = 'bulk';
       
    var url = main + "/app/magic/stock/add";
    var data = { 'card_id': card_id, 'condition': condition, 'language': language, 'group': group, 'stock': stock, 'foil': foil, 'price': price, 'type': type };
    $.ajax({
        method: "POST",
        url: url,
        data: data
    }).done( function(result) {
        console.log(result);
        if ( result.contains("fail") ) {
            window.location.reload(true);
        }
        else {
            var stock_note = button.parent().parent().find('td.Inventory_cell');
            stock_note.html('<span class="hidden">1</span><span class="stock-note instock">' + ( ++previous ) + ' in Stock</span>');
            //button.html("Quick Add");
            button.attr('onclick', 'quickAddStock(\''+ card_id +'\',' + defaultGroup + ', ' +  + previous + ')');
            button.prop('disabled', false);
            button.removeClass('facebook-small');
            
            var grid_stock_note = gridbutton.parent().find('span.stock_container');
            grid_stock_note.html('<span class="hidden">1</span><span class="stock-note instock">' + ( previous ) + ' in Stock</span>');
            //button.html("Quick Add");
            gridbutton.attr('onclick', 'quickAddStock(\''+ card_id +'\',' + defaultGroup + ', ' +  + previous + ')');
            gridbutton.prop('disabled', false);
            gridbutton.removeClass('facebook-small');
            
        }
    });
        
}

function quickUpdate(button, data) {
    
    //button.html("____________");
    button.prop('disabled', true);
    button.addClass('facebook-small');

    var url = main + "/app/magic/stock/mod";
    //var data = { 'card_id': card_id, 'group': group, 'foil': foil, 'price': marketprice, 'all': '1' };
    $.ajax({
        method: "POST",
        url: url,
        data: data
    }).done( function() {
        //window.location.reload(true);
        var price_field = button.parent().parent().find('td.Price_cell');
        price_field.html(accounting.formatMoney( parseFloat(data.fair_price).toFixed(2), { format: "$ %v" }) + '<span class="hidden" id="marketprice_' + data.id + '">' + data.fair_price + '</span>');
        //button.html("Update Price");
        button.prop('disabled', false);
        button.removeClass('facebook-small');
    });

}

/*function getSets(callback) {
    var url = main + '/app/magic/sets/all';

    $.ajax({
        method: "GET",
        url: url,
        dataType: 'json'
    }).success( function(sets) {
        callback(sets);
    });
}*/


